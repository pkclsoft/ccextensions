//
//  CCRadioButton.m
//
//  Created by Peter Easdown on 16/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//
#import "CCRadioButton.h"
#import "CCControlSubclass.h"

@implementation CCRadioButton {
    
    /// local weak references to the three components that make up the visual representation of the switch.
    CCDrawNode *onBackground;
    CCDrawNode *offBackground;
    CCDrawNode *knob;
    
    /// Flag used to prevent repeated calculations to generate the switch components.
    //
    BOOL componentsInitialised;
}

/**
 *  This type is simply used to define the Z order of the components.
 */
typedef enum {
    kZBottomBackground = 10,
    kZTopBackground = 20,
    kZKnob = 30
} CCRadioButtonZOrder;

/**
 *  Basic initialisation; create the basic, empty components.  We don't populate them with anything
 *  until <code>layout</code> is called the first time as we don't know the content size of the 
 *  switch.
 */
- (id) init {
    self = [super init];
    
    if (self != nil) {
        // Setup the ON background so that it is nicely positioned and anchored.  The switch defaults to
        // the OFF state so this component is set to be invisible (opacity = 0) at first.
        //
        onBackground = [CCDrawNode node];
        onBackground.positionType = CCPositionTypeNormalized;
        onBackground.position = cpv(0.5, 0.5);
        onBackground.anchorPoint = cpv(0.5, 0.5);
        onBackground.contentSizeType = CCSizeTypeNormalized;
        onBackground.contentSize = CGSizeMake(1.0, 1.0);
        onBackground.opacity = 0.0;
        
        // Setup the OFF background so that it is nicely positioned and anchored.
        //
        offBackground = [CCDrawNode node];
        offBackground.positionType = CCPositionTypeNormalized;
        offBackground.position = cpv(0.5, 0.5);
        offBackground.anchorPoint = cpv(0.5, 0.5);
        offBackground.contentSizeType = CCSizeTypeNormalized;
        offBackground.contentSize = CGSizeMake(1.0, 1.0);
        
        // Create the knob node
        //
        knob = [CCDrawNode node];
        knob.positionType = CCPositionTypeNormalized;
        knob.position = cpv(0.5, 0.5);
        knob.anchorPoint = cpv(0.5, 0.5);
        knob.contentSizeType = CCSizeTypeNormalized;
        knob.contentSize = CGSizeMake(1.0, 1.0);
        
        // Add the components to self with the appropriate Z order.
        //
        [self addChild:onBackground z:kZTopBackground];
        [self addChild:offBackground z:kZBottomBackground];
        [self addChild:knob z:kZKnob];
        
        // Initialise the colors to nice defaults.
        //
        self.onColor = [CCColor greenColor];
        self.offColor = [CCColor redColor];
        self.knobColor = [CCColor whiteColor];
        
        self.togglesSelectedState = YES;
        
        componentsInitialised = NO;
    }
    
    return self;
}

/**
 *  Given a color, draw an oval filling the contentsize of the specified node.
 *
 *  @param node The node into which to draw the oval.
 *  @param color The color with which to draw.
 */
- (void) drawBackgroundIn:(CCDrawNode*)node withColor:(CCColor*)color {
    float startAngle = 180.0;
    
    // Get the dimensions of the switch.
    //
    float height = self.contentSizeInPoints.height;
    float width = self.contentSizeInPoints.width;

    /// This is the centre of the circle we want points for.  Initially it is the centre of the left side.
    //
    CGPoint center = cpv(width/2.0, height/2.0);
    
    /// We want 72 vertices.  36 for each semicircle.  We want a nice smooth curve.
    //
    CGPoint vertices[72];
    
    // Now create all 72 points.
    //
    for (int pointIndex = 0; pointIndex < 72; pointIndex++) {
        vertices[pointIndex] = [CocosUtil pointOnCircleWithCentre:center andRadius:height/2.0 atDegrees:startAngle];
        startAngle += (360.0 / 72.0);
    }
    
    [node drawPolyWithVerts:vertices count:72 fillColor:color borderWidth:0.0 borderColor:color];
}

/**
 *  Draws the visual of the knob, which is basically a circle with a faint dark border.
 *
 *  @param node The node into which to draw the knob.
 *  @param color The color with which to draw.
 */
- (void) drawKnobInNode:(CCDrawNode*)node withColor:(CCColor*)color {
    float startAngle = 0.0;
    
    // We want the knob to be smaller so that we can see the background around it.
    //
    float height = self.contentSizeInPoints.height * 0.8;
    
    CGPoint center = cpv(self.contentSizeInPoints.width/2.0, self.contentSizeInPoints.height/2.0);
    
    CGPoint vertices[72];
    
    for (int pointIndex = 0; pointIndex < 72; pointIndex++) {
        vertices[pointIndex] = [CocosUtil pointOnCircleWithCentre:center andRadius:height/2.0 atDegrees:startAngle];
        startAngle += (360.0 / 72.0);
    }
    
    CCColor *borderColor = [[CCColor blackColor] colorWithAlphaComponent:0.3];
    [node drawPolyWithVerts:vertices count:72 fillColor:color borderWidth:1.0 borderColor:borderColor];
}

/**
 *  Initialise the visual components.
 */
- (void) initialiseComponents {
    // We only want to do this once.  All other animations are handled by actions.
    //
    if (componentsInitialised == NO) {
        [onBackground clear];
        [offBackground clear];
        [knob clear];
      
        // These calls basically create the visual representation of the switch within the 3 objects
        // we created in init:  We do this now because by the time this method has been called, the
        // switch has been added to the node tree and it's size can be determined.
        //
        // It's one of the downsides of the normalised contentSizeType; that you can't do anything internally
        // based on the size until the object is part of a live node tree.
        //
        [self drawBackgroundIn:offBackground withColor:self.offColor];
        [self drawBackgroundIn:onBackground withColor:self.onColor];
        [self drawKnobInNode:knob withColor:self.knobColor];
        
        componentsInitialised = YES;
    }
}

/**
 *  Overrides the onEnter method so that we can initialise the visual components the first
 *  time the control is realised.
 */
- (void) onEnter {
    [super onEnter];
    
    [self initialiseComponents];
}

// These are the timing constants for the animations between states.
//

/// This is deliberately shorter than the TO_FRONT time so that the layer moving into the
/// background appears to scale down quickly before fading out.
#define TO_BACK_START (0.2)

/// This time is how long it takes to fade out the layer moving into the background.
#define TO_BACK_END (0.4)

/// This is the time taken for the layer becoing the forground to fade and scale in.
#define TO_FRONT (0.4)

/**
 *  A sequence of actions that will appear to move the current foreground into the background.
 */
- (CCActionSequence*) swapToBackSequence {
    return [CCActionSequence actions:
            [CCActionScaleTo actionWithDuration:TO_BACK_START scale:0.9],
            [CCActionSpawn actions:
             [CCActionFadeOut actionWithDuration:TO_BACK_END],
             [CCActionScaleTo actionWithDuration:TO_BACK_END scale:1.0],
             nil],
            nil];
}

/**
 *  A sequence of actions that will appear to move the current background into the forekground.
 */
- (CCActionSequence*) swapToFrontSequence {
    return [CCActionSequence actions:
            [CCActionSpawn actions:
             [CCActionFadeIn actionWithDuration:TO_FRONT],
             [CCActionScaleTo actionWithDuration:TO_FRONT scale:1.0],
             nil],
            nil];
}

/**
 *  A sequence of actions that will move the knob from one position to the other.
 */
- (CCActionSequence*) knobActionSequence {
    return [CCActionSequence actions:
            [CCActionEaseSineOut actionWithAction:
             [CCActionScaleTo actionWithDuration:TO_BACK_END scale:0.4]],
            [CCActionEaseSineOut actionWithAction:
             [CCActionScaleTo actionWithDuration:TO_FRONT scale:1.0]],
            nil];
}

/**
 *  An override of the setSelected: method that animates the switch so that it is a visual representation
 *  of the switches selected state.
 */
- (void) setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected == YES) {
        [offBackground runAction:[self swapToBackSequence]];
        [onBackground runAction:[self swapToFrontSequence]];
        [knob runAction:[self knobActionSequence]];
        
        if (self.radioButtonDelegate != nil) {
            [self.radioButtonDelegate radioButtonDidSelect:self];
        }
    } else {
        [onBackground runAction:[self swapToBackSequence]];
        [offBackground runAction:[self swapToFrontSequence]];
        [knob runAction:[self knobActionSequence]];
    }
}

/**
 *  Returns YES if the switch is considered to be in the ON state.
 */
- (BOOL) isOn {
    return self.selected;
}

/**
 *  Overrides the default behavour to prevent toggling off.
 */
- (void) triggerAction
{
    if (self.selected == NO) {
        self.selected = YES;
        
        if (self.enabled && self.block)
        {
            self.block(self);
        }
    } else {
        [knob runAction:[self knobActionSequence]];
    }
}


@end
