//
//  CocosUtil.m
//
//  Created by Peter Easdown on 23/04/11.
//  Copyright 2011 PKCLSoft. All rights reserved.
//

#import "CocosUtil.h"
#import "math.h"

@implementation CocosUtil

/**
 *  Returns YES if the point <code>test</code> is deemed to be within the polygon described by
 *  the specified vertices.
 *
 *  @param vertices An array of vertices describing a polygon.
 *  @param nvert the number of vertices in the array.
 *  @param test the coordinate to check (must be in the same coordinate space as the polygon).
 *
 *  <b>Note</b> This routine was taken from StackOverflow: http://stackoverflow.com/a/2922778/880807
 */
+ (BOOL)isPointInPolygon:(CGPoint*)vertices count:(NSInteger)nvert point:(CGPoint)test {
    NSInteger c = 0;
    
    for (NSInteger i = 0, j = nvert-1; i < nvert; j = i++) {
        if (((vertices[i].y > test.y) != (vertices[j].y > test.y)) &&
            (test.x < (vertices[j].x - vertices[i].x) *
             (test.y - vertices[i].y) /
             (vertices[j].y - vertices[i].y) +
             vertices[i].x)) {
                c = !c;
            }
    }
    
    return (c ? YES : NO);
}

/**
 *  Converts an angle in the world where 0 is north in a clockwise direction to a world
 *  where 0 is east in an anticlockwise direction.
 *
 *  @param deg The angle in degrees where 0 is north in clockwise direction.
 */
+ (float) angleFromDegrees:(float)deg {
    return fmodf((450.0f - deg), 360.0);
}

/**
 *  Calculates the coordinate of a point on a circle with a specificed radius, centre
 *  and angle where 0.0 is due north (up).
 *
 *  @param centerPt the coordinate of the center of the circle.
 *  @param radius the radius of the circle.
 *  @param degrees the angle in degrees from north (clockwise) at which the point is needed.
 */
+ (CGPoint) pointOnCircleWithCentre:(CGPoint)centerPt andRadius:(float)radius atDegrees:(float)degrees {
    float x = radius + cosf (CC_DEGREES_TO_RADIANS([self angleFromDegrees:degrees])) * radius;
    float y = radius + sinf (CC_DEGREES_TO_RADIANS([self angleFromDegrees:degrees])) * radius;
    
    return ccpAdd(centerPt, ccpSub(CGPointMake(x, y), CGPointMake(radius, radius)));
}

@end


