//
//  CocosUtil.h
//
//  This is a utility class providing a number of convenience functions that assist with
//  Cocos2D animation.  There are a number of functions that are there to help with apps
//  that need to be universal (run on iPhone and iPad).
//
//  Created by Peter Easdown on 23/04/11.
//  Copyright 2011 PKCLsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CocosUtil : NSObject

/**
 *  Converts an angle in the world where 0 is north in a clockwise direction to a world
 *  where 0 is east in an anticlockwise direction.
 *
 *  @param deg The angle in degrees where 0 is north in clockwise direction.
 */
+ (float) angleFromDegrees:(float)deg;

/**
 *  Calculates the coordinate of a point on a circle with a specificed radius, centre
 *  and angle where 0.0 is due north (up).
 *
 *  @param centerPt the coordinate of the center of the circle.
 *  @param radius the radius of the circle.
 *  @param degrees the angle in degrees from north (clockwise) at which the point is needed.
 */
+ (CGPoint) pointOnCircleWithCentre:(CGPoint)centerPt andRadius:(float)radius atDegrees:(float)degrees;

/**
 *  Returns YES if the point <code>test</code> is deemed to be within the polygon described by
 *  the specified vertices.
 *
 *  @param vertices An array of vertices describing a polygon.
 *  @param nvert the number of vertices in the array.
 *  @param test the coordinate to check (must be in the same coordinate space as the polygon).
 */
+ (BOOL)isPointInPolygon:(CGPoint*)vertices count:(NSInteger)nvert point:(CGPoint)test;

@end
