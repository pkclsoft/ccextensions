//
//  CCRadioButtonGroup.m
//
//  Created by Peter Easdown on 18/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import "CCRadioButtonGroup.h"

@implementation CCRadioButtonGroup

/**
 *  Initialises the group with the specified buttons.
 */
- (id) initWithButtons:(NSArray*)buttons {
    self = [super init];
    
    if (self != nil) {
        NSAssert(buttons != nil, @"Attempt to create a CCRadioButtnGroup with nil button array.");
        NSAssert(buttons.count > 1, @"Attempt to create a CCRadioButtnGroup with less than 2 buttons.");
        
        self.buttons = buttons;
        
        for (CCRadioButton* button in self.buttons) {
            button.radioButtonDelegate = self;
        }
    }
    
    return self;
}

/**
 *  Creates a radio button group using the specified array of CCRadioButton objects.
 */
+ (CCRadioButtonGroup*) radioButtonGroupWithButtons:(NSArray*)buttons {
    return [[CCRadioButtonGroup alloc] initWithButtons:buttons];
}

/**
 *  Sets the radio button at the specifed index to ON.
 */
- (void) setSelectedIndex:(int)index {
    int buttonIndex = 0;
    
    for (CCRadioButton* button in self.buttons) {
        if (index == buttonIndex) {
            button.selected = YES;
        } else {
            button.selected = NO;
        }
        
        buttonIndex++;
    }
}

/**
 *  Returns YES if the button at the specified index is ON.
 */
- (BOOL) isButtonAtIndexOn:(int)index {
    return ((CCRadioButton*)[self.buttons objectAtIndex:index]).on;
}

/**
 *  Returns the index of the selected radio button.
 */
- (int) indexOfSelectedButton {
    int result = 0;
    
    for (CCRadioButton* button in self.buttons) {
        if (button.on == YES) {
            break;
        }
        
        result++;
    }
    
    return result;
}


#pragma mark - CCRadioButtonDelegate

/**
 *  Handles selection by a button.
 */
- (void) radioButtonDidSelect:(CCRadioButton *)button {
    for (CCRadioButton* localButton in self.buttons) {
        if ([localButton isEqual:button] == NO) {
            localButton.selected = NO;
        }
    }
}

@end
