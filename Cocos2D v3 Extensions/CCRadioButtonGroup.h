//
//  CCRadioButtonGroup.h
//
//  Created by Peter Easdown on 18/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCRadioButton.h"

/**
 *  A simple container for 2 or more CCRadioButton objects that manages their selection such that
 *  only one is selected at a time.
 */
@interface CCRadioButtonGroup : NSObject <CCRadioButtonDelegate>

/// The buttons being managed by this group.
@property (nonatomic) NSArray *buttons;

/**
 *  Creates a radio button group using the specified array of CCRadioButton objects.
 */
+ (CCRadioButtonGroup*) radioButtonGroupWithButtons:(NSArray*)buttons;

/**
 *  Sets the radio button at the specifed index to ON.
 */
- (void) setSelectedIndex:(int)index;

/**
 *  Returns YES if the button at the specified index is ON.
 */
- (BOOL) isButtonAtIndexOn:(int)index;

/**
 *  Returns the index of the selected radio button.
 */
- (int) indexOfSelectedButton;

@end
