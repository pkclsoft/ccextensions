//
//  CCSwitch.h
//
//  Created by Peter Easdown on 16/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//
#import "CCButton.h"

/**
 *  A CCButton subclass that provides a simple switch visualisation.  Note that this class uses the <code>selected</code>
 *  property of the parent class to manage the on/off state of the switch.
 */
@interface CCSwitch : CCButton

/// This property may be used as an alternative to selected to query the switch state.
@property (nonatomic, readonly) BOOL on;

/// The color to be used to represent the ON state.
@property (nonatomic) CCColor *onColor;

/// The color to be used to represent the OFF state.
@property (nonatomic) CCColor *offColor;

/// The color to be used to represent the knob that moves left/right.
@property (nonatomic) CCColor *knobColor;

@end
