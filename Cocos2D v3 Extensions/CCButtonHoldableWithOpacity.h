//
//  CCButtonHoldableWithOpacity.h
//
//  Created by Peter Easdown on 2/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import "CCButton.h"

/**
 *  A simple subclass of CCButton that will apply it's opacity to all background states so that a button can
 *  be made to fade out or in regardless of it's state.
 *
 *  The button also adds the ability to detect when it is being held down, and when a new touch commences so
 *  that the application can react accordingly.
 */
@interface CCButtonHoldableWithOpacity : CCButton

/**
 *  Indicates whether touch has started within the button.  This will be YES for only the first
 *  call to triggerAction so that the object responding to the touch can detect a new touch.
 */
@property (nonatomic, assign) BOOL touchStarted;

@end
