//
//  CCShapedButton.m
//
//  Created by Peter Easdown on 12/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import "CCShapedButton.h"

@implementation CCShapedButton {
    
    /// The shape of the button as an array of CGPoints which should be in world coordinates.
    CGPoint *_vertices;
    
    /// The number of vertices in the shape.
    int _numVertices;
}

/**
 *  Sets the local reference to the shape of the button as an array of world coordinates that
 *  form a polygon representing the button.
 */
- (void) setShapeVertices:(CGPoint*)vertices count:(int)count {
    _vertices = vertices;
    _numVertices = count;
}

- (BOOL) hitTestWithWorldPos:(CGPoint)pos {
    if (_numVertices > 0) {
        return [CocosUtil isPointInPolygon:_vertices count:_numVertices point:pos];
    } else {
        return NO;
    }
}

@end
