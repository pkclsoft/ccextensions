//
//  CCButtonHoldableWithOpacity.m
//
//  Created by Peter Easdown on 2/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import "CCButtonHoldableWithOpacity.h"
#import "CCControlSubclass.h"

@implementation CCButtonHoldableWithOpacity

/// Whilst the button is being held, it's trigger will fire this often.
#define HELD_TRIGGER_INTERVAL (0.3)

/**
 *  Sets the opacity of the button regardless of state.
 */
- (void) setOpacity:(CGFloat)opacity {
    [super setBackgroundOpacity:opacity forState:CCControlStateNormal];
    [super setBackgroundOpacity:opacity forState:CCControlStateSelected];
    [super setBackgroundOpacity:opacity forState:CCControlStateHighlighted];
    [super setBackgroundOpacity:opacity forState:CCControlStateDisabled];
}

/**
 *  Handles touch enter event, and resets the touchStarted property as appropriate.
 *
 *  If the continuous property is YES. then the triggerAction selector is called periodically
 *  until such time as the touch ends.
 */
- (void) touchEntered:(CCTouch*)touch withEvent:(CCTouchEvent*)event {
    if (!self.enabled) return;
    
    if (self.continuous == YES) {
        self.touchStarted = YES;
        
        [self triggerAction];
        [self schedule:@selector(triggerAction) interval:HELD_TRIGGER_INTERVAL];
    } else {
        [super touchEntered:touch withEvent:event];
    }
    
}

/**
 *  Override of triggerAction to ensure that touchStarted is set to NO to indicate that the next triggerAction
 *  call is a subsequent call.
 */
- (void) triggerAction {
    [super triggerAction];
    
    self.touchStarted = NO;
}

/**
 *  Override of touchUpInside to ensure that the scheduled call of triggerAction for continuous touch is cancelled.
 */
- (void) touchUpInside:(CCTouch*)touch withEvent:(CCTouchEvent*)event {
    if (self.continuous == YES) {
        [self unschedule:@selector(triggerAction)];
    } else {
        [super touchUpInside:touch withEvent:event];
    }
}

/**
 *  Override of touchUpOutside to ensure that the scheduled call of triggerAction for continuous touch is cancelled.
 */
- (void) touchUpOutside:(CCTouch*)touch withEvent:(CCTouchEvent*)event {
    if (self.continuous == YES) {
        [self unschedule:@selector(triggerAction)];
    } else {
        [super touchUpOutside:touch withEvent:event];
    }
}

@end
