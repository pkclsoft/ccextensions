//
//  CCRadioButton.h
//
//  Created by Peter Easdown on 16/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//
#import "CCButton.h"

@protocol CCRadioButtonDelegate;

/**
 *  A CCButton subclass that provides a simple radio button visualisation.  Note that this class uses the <code>selected</code>
 *  property of the parent class to manage the on/off state of the switch.
 */
@interface CCRadioButton : CCButton

/// This property may be used as an alternative to selected to query the switch state.
@property (nonatomic, readonly) BOOL on;

/// The color to be used to represent the ON state.
@property (nonatomic) CCColor *onColor;

/// The color to be used to represent the OFF state.
@property (nonatomic) CCColor *offColor;

/// The color to be used to represent the knob that moves left/right.
@property (nonatomic) CCColor *knobColor;

/// A delegate object used to handle selections.
@property (nonatomic) id<CCRadioButtonDelegate> radioButtonDelegate;

@end

/**
 *  Delegate protocol for CCRadioButton so that delegates can be notified when a button is pressed.
 *  This allows a Radio button group to be implemented.
 */
@protocol CCRadioButtonDelegate

- (void) radioButtonDidSelect:(CCRadioButton*)button;

@end

