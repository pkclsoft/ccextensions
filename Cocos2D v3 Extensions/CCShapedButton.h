//
//  CCShapedButton.h
//
//  Created by Peter Easdown on 12/06/2015.
//  Copyright (c) 2015 PKCLsoft. All rights reserved.
//

#import "CCButton.h"

/**
 *  Subclass of CCButton that uses a polygon to define the hit region for touches.
 */
@interface CCShapedButton : CCButton

/**
 *  Sets the local reference to the shape of the button as an array of world coordinates that
 *  form a polygon representing the button.
 */
- (void) setShapeVertices:(CGPoint*)vertices count:(int)count;

@end
