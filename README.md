# README #

This repository contains a number of classes I've put together to make life easier as I work on apps using Cocos2D.

### How do I get set up? ###

There are, for now, no tests or sample projects; just the standalone code.  As time wears on and I add more here
I'll endeavour to add a project however I'd rather not provide an entire copy of Cocos2D here.

For v3, make sure you grab CocosUtil as well as the classes you need as it contains helper methods.

### Contribution guidelines ###

* Writing tests
* Code review
